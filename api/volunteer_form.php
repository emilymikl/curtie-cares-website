<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $formdata = json_decode(file_get_contents("php://input"), true);

    if(!empty($formdata)) {
        $name = $formdata['name'];
        $message = $formdata['message'];
        $phone = $formdata['phone'];
        $email = $formdata['email'];

        $volunteerData = array(
            'name' => $name,
            'message' => $message,
            'phone' => $phone,
            'email' => $email
        );

        $this.sendEmail($volunteerData);
    }

    function sendEmail($volunteerData) {
        $message = 'Hello Curtie! Someone is interested in volunteering!' . "\r\n" . "\r\n" . 'Here is their contact info:' . "\r\n" . "\r\n";
        $message .= 'Name: '.$volunteerData['name'].'' . "\r\n";
        $message .= 'Message: '.$volunteerData['message'].'' . "\r\n";

        if(!empty($volunteerData['phone'])) {
            $message .= 'Phone: '.$volunteerData['phone'].'' . "\r\n";
        }

        $message .= 'Email: '.$volunteerData['email'].'' . "\r\n";

        $to      = 'info@curtiecares.org';
        $subject = 'Volunteer form submission from website';
        $headers = 'From: info@curtiecares.org' . "\r\n" .
            'Reply-To: info@curtiecares.org' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
    }
?>
