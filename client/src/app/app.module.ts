import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FlexLayoutModule} from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { AboutModule } from './about/about.module';
import { FaqModule } from './faq/faq.module';
import { ContactModule } from './contact/contact.module';
import { HomeModule } from './home/home.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from '@angular/material/menu';
import {VolunteerModule} from "./volunteer/volunteer.module";

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FlexLayoutModule,
        AboutModule,
        FaqModule,
        ContactModule,
        HomeModule,
        VolunteerModule,
        BrowserAnimationsModule,
        MatMenuModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
