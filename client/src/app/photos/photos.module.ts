import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotosComponent } from './photos.component';
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    PhotosComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'photos', component: PhotosComponent, pathMatch: 'full'}
    ])
  ],
  exports: [RouterModule]
})
export class PhotosModule { }
