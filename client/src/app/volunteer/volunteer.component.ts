import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ErrorStateMatcher} from "@angular/material/core";
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Volunteer, VolunteerService} from "./volunteer.service";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-volunteer',
  templateUrl: './volunteer.component.html',
  styleUrls: ['./volunteer.component.less']
})
export class VolunteerComponent implements OnInit {

  @ViewChild('message') messageDiv: ElementRef;

  volunteerForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    secondary: new FormControl(''),
    name: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required)
  });

  submitted = false;
  error: HttpErrorResponse;
  data: any;
  honeypot = false;

  constructor(private volunteerService: VolunteerService) { }

  emailValidationMessage() {
    return this.volunteerForm.get('email').hasError('required') ? 'Please enter an email' :
      this.volunteerForm.get('email').hasError('email') ? 'Not a valid email' : '';
  }

  nameValidationMessage() {
    return this.volunteerForm.get('name').hasError('required') ? 'Please enter your name' : '';
  }

  phoneValidationMessage() {
    return this.volunteerForm.get('phone').hasError('required') ? 'Please enter a phone number' : '';
  }

  ngOnInit() {}

  onSubmit() {
    if (this.volunteerForm.valid && this.volunteerForm.get('secondary').value) {
      this.submitted = true;
      this.honeypot = true;
    }
    if (this.volunteerForm.valid && !this.honeypot) {
      this.submitted = true;
      return this.volunteerService.volunteerForm(this.volunteerForm.value).subscribe((res: Volunteer) => {
        this.data = res;
        this.messageDiv.nativeElement.focus();
      }, (error: HttpErrorResponse) => {
        this.error = error;
        this.messageDiv.nativeElement.focus();
      });
    }
  }

}
