import { Injectable} from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

export class Volunteer {
  name: string;
  email: string;
  phone: string;
}


@Injectable({
  providedIn: 'root'
})
export class VolunteerService {
  errorMessage: string;
  data: any;

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})
  };

  constructor(private http: HttpClient) {}

  getData(): Observable<any> {
    return this.http.get('/api/volunteer_form.php');
  }

  volunteerForm(formdata: Volunteer): Observable<Volunteer> {
    return this.http.post<Volunteer>('/api/volunteer_form.php',
      formdata, this.httpOptions).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred: ', error.error.message);
    } else {
      console.error(`Response was  + ${error.status},` + ` body was:  + ${error.error}`);
    }

    this.errorMessage = 'Oops! Something went wrong. Please try again later.';

    return throwError(this.errorMessage);
  }
}
